/* Includes -------------------------------------------------------------------------------------*/
#include "hil_utils.h"
#include <string.h>
/* Private typedef ------------------------------------------------------------------------------*/
/* Private define -------------------------------------------------------------------------------*/
/* Private macro --------------------------------------------------------------------------------*/
/* Private variables ----------------------------------------------------------------------------*/
/* Private function prototypes ------------------------------------------------------------------*/
static void hil_utils_reverse( char *str, int length );
/* Private functions ----------------------------------------------------------------------------*/

/**------------------------------------------------------------------------------------------------
Brief.- strtok_r emulation to be safe thrade compatible with rtos, this is not a full implementation
        the function does not make a parameter validation.
        to keep tokenisation over the same string is mandatory to call the function with a NULL
        parameter
Param.- str: string to tokenaize
Param.- delim: character to use as token, this is not a string is a single character
Param.- save: pointer to save where the string is being tokenise
Return.- retrun the adreess where the chunk beging, NULL is no token is found 
-------------------------------------------------------------------------------------------------*/
char *hil_utils_strtok( char *str, const char delim, char** save )
{
    char *ptr;
    
    /*if this is not the first time, use save pointer*/
    if( str == NULL )
    {
        str = *save;
    }
    else
    {
        /*if this is the first time save pointer should be null*/
        *save = NULL;
    }
    
    /*look for the character in the string*/
    ptr = strchr( str, delim );
    if( ptr != NULL )
    {
        /*found a character*/
        *ptr = '\0';
        *save = ++ptr;
    }
    else if( *save == NULL )
    {
        /*if not probably the character is not in the entire string*/
        str = NULL;
    }
    
    return str;
}


/**------------------------------------------------------------------------------------------------
Brief.- strtok_r emulation to be safe thrade compatible with rtos, this is not a full implementation
        the function does not make a parameter validation.
        to keep tokenisation over the same string is mandatory to call the function with a NULL
        parameter
Param.- num: number to convert to string
Param.- str: pointer where the string will be stored
Param.- base: base number convertion, 10, 16 or 2
Return.- address where the tring is stored  smae as parameter str
-------------------------------------------------------------------------------------------------*/
char* hil_utils_itoa( int num, char* str, int base )  
{
    int rem, i = 0; 
    int negative = 0u; 
  
    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if( num == 0 ) 
    { 
        str[ i++ ] = '0'; 
        str[ i ] = '\0'; 
        return str; 
    } 
  
    // In standard itoa(), negative numbers are handled only with  
    // base 10. Otherwise numbers are considered unsigned. 
    if( ( num < 0 ) && ( base == 10 ) ) 
    { 
        negative = 1u; 
        num *= -1; 
    } 
  
    // Process individual digits 
    while( num != 0 ) 
    { 
        rem = num % base; 
        //str[ i++ ] = ( rem > 9 ) ? ( rem-10 ) + 'a' : rem + '0'; 
        if( rem > 9 )
        {
            str[ i++ ] = ( rem - 10 ) + 'a';
        }
        else
        {
            str[ i++ ] = rem + '0';
        }
        
        num = num / base; 
    } 
  
    // If number is negative, append '-' 
    if( negative )
    {
        str[ i++ ] = '-';
    } 
  
    str[ i ] = '\0'; // Append string terminator 
  
    // Reverse the string 
    hil_utils_reverse( str, i ); 
  
    return str; 
}


static void hil_utils_reverse( char *str, int length ) 
{ 
    int start = 0; 
    int end = length - 1;
    char temp;

    while( start < end ) 
    { 
        temp = str[ start ];
        str[ start ] = str[ end ];
        str[ end ] = temp;
        start++; 
        end--; 
    } 
}
