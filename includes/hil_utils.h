/**------------------------------------------------------------------------------------------------
  * @author  Diego Perez
  * @version V1.0.0
  * @date    06-June-2020
  * @brief   File to define and declare (extern) symbols to be used across the project
  *          A good example will be global variables and definitios for pin out and etc ..
-------------------------------------------------------------------------------------------------*/

#ifndef __hil_utils_h__
#define __hil_utils_h__

/* Includes -------------------------------------------------------------------------------------*/
/* Global typedef ------------------------------------------------------------------------------*/
/* Global define -------------------------------------------------------------------------------*/
/* Global macro --------------------------------------------------------------------------------*/
/* Global variables ----------------------------------------------------------------------------*/
/* Global function prototypes ------------------------------------------------------------------*/
extern char *hil_utils_strtok( char *str, const char delim, char** save );
extern char *hil_utils_itoa( int num, char* str, int base );

#endif
